package com.compositeapps.gia;

import com.compositeapps.gia.utils.EncryptUtil;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.properties.EncryptableProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.Executor;


@EnableAsync
@EnableCaching
@EntityScan("com.compositeapps.gia")
@SpringBootApplication
@EnableJpaRepositories("com.compositeapps.gia.repositories")
public class GiaApplication extends SpringBootServletInitializer {

    @Value("${spring.datasource.driverClassName}")
    private String databaseDriverClassName;

    @Value("${spring.datasource.url}")
    private String datasourceUrl;

    @Value("${spring.datasource.username}")
    private String databaseUsername;

    @Value("${spring.profiles.active}")
    private String activeProfile;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(GiaApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(GiaApplication.class, args);
    }

    @Bean
    public Executor asyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("GIAEmailScan-");
        executor.initialize();
        return executor;
    }

    @Bean
    public DataSource dataSource() throws IOException {
        return DataSourceBuilder.create()
                .username(databaseUsername)
                .password(EncryptUtil.getEncryptedVaue(activeProfile, "spring.datasource.password"))
                .url(datasourceUrl)
                .driverClassName(databaseDriverClassName)
                .build();
    }

}
