package com.compositeapps.gia.controllers;


import com.compositeapps.gia.models.*;
import com.compositeapps.gia.models.Outlook.OutlookUser;
import com.compositeapps.gia.models.Outlook.TokenResponse;
import com.compositeapps.gia.models.SalesForce.IdToken;
import com.compositeapps.gia.services.spring.EmailListenerService;
import com.compositeapps.gia.services.retrofit.*;
import com.compositeapps.gia.utils.AuthHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.ui.Model;


@RestController
@RequestMapping("/email")
public class EmailController {
    private static final String VERSION = "1.3.1";

    private EmailListenerService emailListenerService;

    @Autowired
    public EmailController(EmailListenerService emailListenerService) {
        this.emailListenerService = emailListenerService;
    }

    @GetMapping("/run")
    public String loginTest(Model model, HttpServletRequest request, HttpServletResponse response) {
        UUID state = UUID.randomUUID();
        UUID nonce = UUID.randomUUID();

        // Save the state and nonce in the session so we can
        // verify after the auth process redirects back
        HttpSession session = request.getSession();
        session.setAttribute("expected_state", state);
        session.setAttribute("expected_nonce", nonce);

        String loginUrl = AuthHelper.getLoginUrl(state, nonce);
        model.addAttribute("loginUrl", loginUrl);
        // Name of a definition in WEB-INF/defs/pages.xml
        try {
            response.sendRedirect(loginUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "redirect:" + loginUrl;
    }

    @PostMapping(value = "/authorize")
    public String authorize(
            @RequestParam("code") String code,
            @RequestParam("id_token") String idToken,
            @RequestParam("state") UUID state,
            HttpServletRequest request) {

        // Get the expected state value from the session
        HttpSession session = request.getSession();
        UUID expectedState = (UUID) session.getAttribute("expected_state");
        UUID expectedNonce = (UUID) session.getAttribute("expected_nonce");

        // Make sure that the state query parameter returned matches
        // the expected state
        if (state.equals(expectedState)) {
            IdToken idTokenObj = IdToken.parseEncodedToken(idToken, expectedNonce.toString());
            if (idTokenObj != null) {
                UserSession userSession = UserSession.getInstance();
                TokenResponse tokenResponse = AuthHelper.getTokenFromAuthCode(code, idTokenObj.getTenantId());

                // favor singleton model over http session
                userSession.setTokenResponse(tokenResponse);
                userSession.setIsConnected(true);
                userSession.setUserName(idTokenObj.getName());

                // Get user info
                OutlookService outlookService = OutlookServiceBuilder.getOutlookService(tokenResponse.getAccessToken(), null);
                OutlookUser user;
                try {
                    user = outlookService.getCurrentUser().execute().body();
                    userSession.setUserEmail(idTokenObj.getPreferredUsername());
                } catch (IOException e) {}

                // session.setAttribute("userTenantId", idTokenObj.getTenantId());
                userSession.setTenantId(idTokenObj.getTenantId());
            } else {
                // TODO: Handle  error "error", "ID token failed validation."
                return "Unable to start service: ID token failed validation";
            }
        }
        else {
            // TODO: Handle  error "error", "Unexpected state returned from authority."
            return "Unable to start service: Unexpected state returned from authority";
        }
        try {
            emailListenerService.start();
        } catch(Exception e) {
            e.getMessage();
            return "Unable to start service";
        }

        return "Service started for Outlook account " + UserSession.getInstance().getUserEmail() + " - V." + VERSION;
    }

    @GetMapping("/status")
    public String serviceStatus(HttpServletRequest request) {
        String serviceStatus = "Email Service is not running";
        if (emailListenerService.isRunning()) {
            serviceStatus = "Email Service is running";
        }
        return serviceStatus;
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.invalidate();
        UserSession.destroy();
        emailListenerService.destroy();
        return "Logging out...";
    }
}
