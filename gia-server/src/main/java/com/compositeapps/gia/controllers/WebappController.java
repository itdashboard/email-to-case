package com.compositeapps.gia.controllers;

import com.compositeapps.gia.enums.EmailAlias;
import com.compositeapps.gia.models.API.ApiError;
import com.compositeapps.gia.models.API.ApiSuccess;
import com.compositeapps.gia.models.OutboundEmailRequest;
import com.compositeapps.gia.services.spring.EmailService;
import com.compositeapps.gia.services.spring.OutlookEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/email")
public class WebappController {

    @Autowired private EmailService emailService;

    /**
     * POST - Sends an outgoing email
     * @param request The email request
     * @return 400 BAD REQUEST if invalid parameters, otherwise 200 OK if email was sent without issues
     */
    @PostMapping("/send")
    public ResponseEntity<Object> sendOutboundEmail(@RequestBody @Valid OutboundEmailRequest request) {

        String fromAddress = request.getFromAddress();
        if (fromAddress == null || fromAddress.isEmpty()) {
            return ResponseEntity.badRequest().body(new ApiError(HttpStatus.BAD_REQUEST, "Please specify a GIA alias to send the mail as."));
        } else if (EmailAlias.isAlias(fromAddress)) {
            return ResponseEntity.badRequest().body(new ApiError(HttpStatus.BAD_REQUEST, "The email '" + fromAddress + " is not  a valid GIA alias."));
        }

        String[] toRecipients = request.getToRecipients();
        if (toRecipients == null || toRecipients.length == 0) {
            return ResponseEntity.badRequest().body(new ApiError(HttpStatus.BAD_REQUEST, "You must specify at least 1 recipient to send the email to."));
        }

        String subject = request.getSubject();
        if (subject == null || subject.isEmpty()) {
            return ResponseEntity.badRequest().body(new ApiError(HttpStatus.BAD_REQUEST, "Missing required field 'subject'."));
        }

        String body = (request.getHtmlBody() == null) ? request.getTextBody() : request.getHtmlBody();
        if (body == null || body.isEmpty()) {
            return ResponseEntity.badRequest().body(new ApiError(HttpStatus.BAD_REQUEST, "Missing required field 'body'. Please include the HTML body or text body of the email."));
        }

        try {
            this.emailService.sendEmail(request);
        } catch (MessagingException e) {
            return ResponseEntity.badRequest().body(new ApiError(HttpStatus.BAD_REQUEST, e.getMessage()));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()));
        }

        return ResponseEntity.ok().body(new ApiSuccess(HttpStatus.OK, "Email sent successfully"));
    }


}
