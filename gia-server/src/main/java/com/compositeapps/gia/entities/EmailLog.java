package com.compositeapps.gia.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "email_log")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class EmailLog {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;

    @Column(name = "date_sent")
    private LocalDateTime dateSent;

    @Lob
    @Column(name = "body")
    private String body;

    @Lob
    @Column(name = "from_email")
    private String from;

    @Column(name = "email_alias")
    private String emailAlias;

    @Lob
    @Column(name = "recipients")
    private String recipients;

    @Lob
    @Column(name = "subject")
    private String subject;

    @Lob
    @Column(name = "header")
    private String header;

    @Column(name = "is_reply")
    private Boolean isReply;

    @Column(name = "is_automated")
    private Boolean isAutomated;

    @Column(name = "is_read")
    private Boolean isRead;

    @Column(name = "total_attachments")
    private Integer totalAttachments;

    @Lob
    @Column(name = "thread_id")
    private String threadId;

    @Lob
    @Column(name = "case_id")
    private String caseId;

    @Lob
    @Column(name = "case_no")
    private String caseNo;

    @Lob
    @Column(name = "message_id")
    private String messageId;

    @Column(name = "sender_ip")
    private String senderIp;

    public void setIsRead(boolean read) {
        this.isRead = read;
    }

}
