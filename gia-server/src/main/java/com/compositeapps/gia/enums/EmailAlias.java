package com.compositeapps.gia.enums;

public enum EmailAlias {


    GIA_SALESFORCE("giasalesforce@gia.edu"),
    SAM_GIA_TEST("samgiatest@outlook.com"),

    ANTWERP("antwerplab@gia.edu"),
    BANGKOK("bangkoklab@gia.edu"),
    BOTSWANA("gaboronelab@gia.edu"),
    CARLSBAD("carlsbadlab@gia.edu"),
    DUBAI("dubailab@gia.edu"),
    HONG_KONG("hongkonglab@gia.edu"),
    MUMBAI("mumbailab@gia.edu"),
    NEW_YORK("newyorklab@gia.edu"),
    RAMAT_GAN("ramatganlab@gia.edu"),
    SOUTH_AFRICA("johannesburglab@gia.edu"),
    SURAT("suratlab@gia.edu"),
    TOKYO("tokyolab@gia.edu");

    private final String aliasName;

    EmailAlias(String name) {
        this.aliasName = name;
    }

    public String toString() {
        return aliasName;
    }

    public static Boolean isAlias(String email) {
        for (EmailAlias alias : EmailAlias.values()) {
            if (alias.name().equalsIgnoreCase(email)) {
                return true;
            }
        }
        return false;
    }

}
