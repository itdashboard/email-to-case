package com.compositeapps.gia.models.API;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ApiError {

    private ApiResponse error;

    public ApiError(HttpStatus status, String message) {
        this.error = new ApiResponse(status, message);
    }

}
