package com.compositeapps.gia.models.API;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ApiResponse {

    private Integer code;
    private String status;
    private String message;

    public ApiResponse(HttpStatus status, String message) {
        this.code    = status.value();
        this.status  = status.name();
        this.message = message;
    }

}
