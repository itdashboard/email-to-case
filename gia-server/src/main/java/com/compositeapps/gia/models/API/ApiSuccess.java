package com.compositeapps.gia.models.API;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ApiSuccess {

    private ApiResponse success;

    public ApiSuccess(HttpStatus status, String message) {
        this.success = new ApiResponse(status, message);
    }

}
