package com.compositeapps.gia.models;

import lombok.Data;

@Data
public class MoveMessageRequest {
    private String destinationId;
}
