package com.compositeapps.gia.models;

import lombok.Data;

@Data
public class OutboundEmailRequest {

    private String[] toRecipients;
    private String[] bccRecipients;
    private String fromAddress;
    private String subject;
    private String htmlBody;
    private String textBody;
    private String[] attachments;

}
