package com.compositeapps.gia.models.Outlook;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class OutgoingMessage {
    @JsonProperty("message")
    private OutgoingMessageContent message;
}
