package com.compositeapps.gia.models.Outlook;

import com.compositeapps.gia.models.SalesForce.MessageBodyContent;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class OutgoingMessageContent {

    @JsonProperty("subject")
    private String subject;

    @JsonProperty("importance")
    private String importance;

    @JsonProperty("body")
    private MessageBodyContent body;

    @JsonProperty("toRecipients")
    private Recipient[] toRecipients;

    @JsonProperty("ccRecipients")
    private Recipient[] ccRecipients;

}
