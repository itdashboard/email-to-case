package com.compositeapps.gia.models.Outlook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.sql.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class OutlookAttachment {
    private String id;
    private Date lastModifiedDateTime;
    private String contentType;
    private String name;
    private Boolean isInline;
    private String contentId;
    private String contentLocation;
    private String contentBytes;
}
