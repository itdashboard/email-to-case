package com.compositeapps.gia.models.Outlook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class OutlookUser {
    private String id;
    private String mail;
    private String displayName;
}
