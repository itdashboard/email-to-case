package com.compositeapps.gia.models.Outlook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PagedResult<T> {
    @JsonProperty("@odata.nextLink")
    @JsonIgnoreProperties(ignoreUnknown = true)
    private String nextPageLink;
    private T[] value;

}
