package com.compositeapps.gia.models.Outlook;

import com.compositeapps.gia.models.SalesForce.EmailAddress;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Recipient {
    private EmailAddress emailAddress;
}
