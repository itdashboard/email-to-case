package com.compositeapps.gia.models.Outlook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInfo {

    @JsonProperty("user_id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("email")
    private String email;

    @JsonProperty("preferred_username")
    private String username;

    @JsonProperty("organization_id")
    private String organizationId;

}
