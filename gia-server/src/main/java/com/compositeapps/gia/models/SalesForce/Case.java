package com.compositeapps.gia.models.SalesForce;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Case {

    @JsonProperty("Id")
    private String id;

    @JsonProperty("Type")
    private String type;

    @JsonProperty("Origin")
    private String origin;

    @JsonProperty("Reason")
    private String reason;

    @JsonProperty("Status")
    private String status;

    @JsonProperty("OwnerId")
    private String ownerId;

    @JsonProperty("Subject")
    private String subject;

    @JsonProperty("IsClosed")
    private Boolean isClosed;

    @JsonProperty("CaseNumber")
    private String caseNumber;

    @JsonProperty("Priority")
    private String priority;

    @JsonProperty("ContactId")
    private String contactId;

    @JsonProperty("ContactEmail")
    private String contactEmail;

    @JsonProperty("ContactPhone")
    private String phone;

    @JsonProperty("AccountId")
    private String accountId;

    @JsonProperty("Description")
    private String description;

    @JsonProperty("SuppliedName")
    private String suppliedName;

    @JsonProperty("SuppliedEmail")
    private String suppliedEmail;

    @JsonProperty("RecordTypeId")
    private String recordTypeId;

    @JsonProperty("Email_Alias__c")
    private String emailAlias;

    @JsonProperty("Identity")
    private String identity;

}
