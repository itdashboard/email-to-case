package com.compositeapps.gia.models.SalesForce;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Contact {
    @JsonProperty("Id")
    private String id;

    @JsonProperty("AccountId")
    private String accountId;

    @JsonProperty("Email")
    private String email;

    @JsonProperty("Account")
    private Account account;

    @JsonProperty("FirstName")
    private String firstName;

    @JsonProperty("LastName")
    private String lastName;

    @JsonProperty("RecordTypeId")
    private String recordTypeId;

    @JsonProperty("Lab_Contact_Type__c")
    private String labContactType;

}
