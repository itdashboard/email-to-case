package com.compositeapps.gia.models.SalesForce;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailAddress {
    private String name;
    private String address;
}
