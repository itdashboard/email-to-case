package com.compositeapps.gia.models.SalesForce;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MailFolder {
    private String id;
    private String displayName;
    private String parentFolderId;
    private Integer childFolderCount;
    private Integer unreadItemCount;
    private Integer totalItemCount;
}
