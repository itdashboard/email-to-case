package com.compositeapps.gia.models.SalesForce;

import com.compositeapps.gia.models.Outlook.Recipient;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.sql.Date;
import java.util.ArrayList;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Message {
    private String id;
    private Date receivedDateTime;
    private Recipient from;
    private ArrayList<Recipient> ToRecipients;
    private Object InternetMessageHeaders;
    private Boolean isRead;
    private Boolean hasAttachments;
    private String subject;
    private String bodyPreview;
    private MessageBodyContent body;
    private String parentFolderId;
}
