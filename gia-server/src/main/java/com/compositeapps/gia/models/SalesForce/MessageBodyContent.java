package com.compositeapps.gia.models.SalesForce;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MessageBodyContent {

    @JsonProperty("contentType")
    private String contentType;

    @JsonProperty("content")
    private String content;
}
