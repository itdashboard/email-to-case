package com.compositeapps.gia.models.SalesForce;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SFAttachment {

    @JsonProperty("Body")
    private String body;

    @JsonProperty("Name")
    private String name;

    @JsonProperty("ParentId")
    private String parentId;

    @JsonProperty("ContentType")
    private String contentType;

}
