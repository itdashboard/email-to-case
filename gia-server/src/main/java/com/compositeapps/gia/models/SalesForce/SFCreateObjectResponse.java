package com.compositeapps.gia.models.SalesForce;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class SFCreateObjectResponse {

    @JsonProperty("id")
    private String id;

    @JsonProperty("errors")
    private SFError[] errors;

    @JsonProperty("success")
    private Boolean success;

}
