package com.compositeapps.gia.models.SalesForce;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SFEmailMessage {

    @JsonProperty("Status")
    private String status;

    @JsonProperty("Headers")
    private String headers;

    @JsonProperty("Subject")
    private String subject;

    @JsonProperty("FromName")
    private String fromName;

    @JsonProperty("HtmlBody")
    private String htmlBody;

    @JsonProperty("TextBody")
    private String textBody;

    @JsonProperty("ToAddress")
    private String toAddress;

    @JsonProperty("FromAddress")
    private String fromAddress;

    @JsonProperty("CCAddress")
    private String ccAddress;

    @JsonProperty("MessageDate")
    private String messageDate;

    @JsonProperty("ParentId")
    private String parentId;

    @JsonProperty("ThreadIdentifier")
    private String threadIdentifier;

    @JsonProperty("Incoming")
    private Boolean incoming;

}
