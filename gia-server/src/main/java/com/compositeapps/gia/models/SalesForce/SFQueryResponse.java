package com.compositeapps.gia.models.SalesForce;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SFQueryResponse<T> {

    @JsonProperty("totalSize")
    private Integer totalSize;

    @JsonProperty("done")
    private Boolean done;

    @JsonProperty("records")
    private T[] records;

}
