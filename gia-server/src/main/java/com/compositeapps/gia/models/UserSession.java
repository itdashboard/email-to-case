package com.compositeapps.gia.models;

import com.compositeapps.gia.models.Outlook.TokenResponse;
import com.compositeapps.gia.models.SalesForce.Message;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserSession {
    private static UserSession instance = null;

    private final String SF_SANDBOX_ORG_ID = "00D1k0000008cty";
    private final String SF_PROD_ORG_ID    = "00D30000000muZZ";

    private TokenResponse tokenResponse;
    private Boolean isConnected;
    private String userName;
    private String userEmail;
    private String tenantId;
    private List<Message> inboxMessages = new ArrayList<>();
    private List<Message> sentMessages = new ArrayList<>();
    private String SFToken;
    private String SFUrl;
    private String SFUserId;
    private String SFOrganizationId;
    // private List<Message> messages = new ArrayList<>();
    private List<Message> messageHistory = new ArrayList<>();

    protected UserSession() {
        // Exists only to defeat instantiation.
    }

    public static UserSession getInstance() {
        if(instance == null) {
            instance = new UserSession();
        }
        return instance;
    }

    public static void destroy() {
        instance = null;
    }

    public static void addInboxMessage(Message message) {
        instance.inboxMessages.add(message);
    }

    public static void addSentMessage(Message message) {
        instance.sentMessages.add(message);
    }

    public static TokenResponse getTokenResponse() { return instance.tokenResponse; }

    public static Boolean getIsConnected() { return instance.isConnected; }

    public static String getUserEmail() { return instance.userEmail; }

    public static String getUserName() { return instance.userName; }

    public static String getTenantId() { return instance.tenantId; }

    // public static List<Message> getMessages() { return instance.messages; }

    public static List<Message> getMessageHistory() { return instance.messageHistory; }

    public String getSFToken() {
        return SFToken;
    }

    public void setSFToken(String SFToken) {
        this.SFToken = SFToken;
    }

    public String getSFUrl() {
        return SFUrl;
    }

    public void setSFUrl(String SFUrl) {
        this.SFUrl = SFUrl;
    }

    public String getSFUserId() {
        return SFUserId;
    }

    public void setSFUserId(String SFUserId) {
        this.SFUserId = SFUserId;
    }
}
