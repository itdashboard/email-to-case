package com.compositeapps.gia.repositories;

import com.compositeapps.gia.entities.EmailLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmailLogRepository extends JpaRepository<EmailLog,Long> {
    List<EmailLog> findByCaseNoOrderByDateSentDesc(String caseNo);
    List<EmailLog> findByThreadIdOrderByDateSentDesc(String threadId);
}
