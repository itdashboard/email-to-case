package com.compositeapps.gia.services.retrofit;

import com.compositeapps.gia.models.*;
import com.compositeapps.gia.models.Outlook.OutgoingMessage;
import com.compositeapps.gia.models.Outlook.OutlookAttachment;
import com.compositeapps.gia.models.Outlook.OutlookUser;
import com.compositeapps.gia.models.Outlook.PagedResult;
import com.compositeapps.gia.models.SalesForce.MailFolder;
import com.compositeapps.gia.models.SalesForce.Message;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;

public interface OutlookService {
    public final String API_VERSION = "v1.0";

    @GET("/" + API_VERSION + "/me")
    Call<OutlookUser> getCurrentUser();

    @GET("/" + API_VERSION + "/me/messages")
    Call<PagedResult<Message>> getMessages(
            @Query("$orderby") String orderBy,
            @Query("$select") String select,
            @Query("$filter") String filter,
            @Query("$top") Integer maxResults
    );

    @GET("/" + API_VERSION + "/me/messages/{id}/attachments")
    Call<PagedResult<OutlookAttachment>> getMessageAttachments(@Path("id") String messageId);

    @POST("/" + API_VERSION + "/me/sendMail")
    Call<Message> sendMessage(@Body OutgoingMessage request);

    @POST("/" + API_VERSION + "/me/messages/{id}/move")
    Call<Message> moveMessage(@Path("id") String messageId, @Body MoveMessageRequest request);

    @PATCH("/" + API_VERSION + "/me/messages/{id}")
    Call<Message> updateMessage(@Path("id") String messageId, @Body Message request);

    @GET("/" + API_VERSION + "/me/mailFolders")
    Call<PagedResult<MailFolder>> getAllMailFolders();

    @GET("/" + API_VERSION + "/me/mailFolders/{id}")
    Call<MailFolder> getMailFolderById(@Path("id") String folderId);

    @POST("/" + API_VERSION + "/me/mailFolders/")
    Call<MailFolder> createFolder(@Body MailFolder request);

    @DELETE("/" + API_VERSION + "/me/messages/{id}")
    Call<ResponseBody>  deleteMessage(
            @Path("id") String messageId
    );

}
