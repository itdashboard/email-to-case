package com.compositeapps.gia.services.retrofit;

import com.compositeapps.gia.models.Outlook.UserInfo;
import com.compositeapps.gia.models.SalesForce.*;
import retrofit2.Call;
import retrofit2.http.*;


public interface SalesForceService {

    public final String API_VERSION = "v1.0";   // TODO - Upgrade to 2.0; 1.0 is being depricated

    @POST("services/oauth2/token")
    Call<SFTokenResponse> authenticateUser(
            @Query("grant_type") String grantType,
            @Query("client_id") String clientId,
            @Query("client_secret") String clientSecret,
            @Query("username") String username,
            @Query("password") String password);

    @POST("/services/data/v39.0/sobjects/Case")
    Call<SFCreateObjectResponse> createCase(@Body Case request);

    @GET("/services/data/v39.0/query/")
    Call<SFQueryResponse<Case>> findCaseByThreadId(@Query("q") String threadIdQuery);

    @GET("/services/data/v39.0/sobjects/Case/{caseId}")
    Call<Case> findCaseById(@Path("caseId") String caseId);

    @GET("/services/oauth2/userinfo")
    Call<UserInfo> getCurrentUserInfo();

    @GET("/services/data/v39.0/sobjects/EmailMessage/{emailMessageId}")
    Call<SFEmailMessage> findEmailMessageById(@Path("emailMessageId") String emailMessageId);

    @GET("/services/data/v39.0/query/")
    Call<SFQueryResponse<Contact>> getContactsByEmail(@Query("q") String contactsQuery);

    @POST("/services/data/v39.0/sobjects/Attachment")
    Call<SFCreateObjectResponse> createAttachment(@Body SFAttachment request);

    @POST("/services/data/v39.0/sobjects/EmailMessage")
    Call<SFCreateObjectResponse> createEmailMessage(@Body SFEmailMessage request);

    @POST("/services/data/v39.0/sobjects/Contact")
    Call<SFCreateObjectResponse> createContact(@Body Contact request);

  /*
    @DELETE("/" + API_VERSION + "/me/messages/{id}")
    Call<ResponseBody>  deleteMessage(
            @Path("id") String messageId
    );*/

}
