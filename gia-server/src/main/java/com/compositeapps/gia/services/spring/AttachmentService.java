package com.compositeapps.gia.services.spring;

import com.compositeapps.gia.models.SalesForce.SFAttachment;
import com.compositeapps.gia.models.SalesForce.SFCreateObjectResponse;
import com.compositeapps.gia.services.retrofit.SalesForceServiceBuilder;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class AttachmentService extends SFService {

    public String createAttachment(SFAttachment attachment) {
        String tokens = session.getSFToken();
        if (tokens == null) {
            callGetToken();
        }

        try {
            SFCreateObjectResponse response = SalesForceServiceBuilder.getSalesForceService(session.getSFToken(), session.getSFUrl()).createAttachment(attachment).execute().body();
            if (response != null) {
                if (response.getSuccess()) {
                    return response.getId();

                } else {
                    printSFCreateObjectError(response, "Error(s) creating attachment");
                }
            }

        } catch (IOException e) {
            return null;
        }

        return null;
    }

}
