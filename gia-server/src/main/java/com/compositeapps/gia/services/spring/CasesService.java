package com.compositeapps.gia.services.spring;

import com.compositeapps.gia.enums.EmailAlias;
import com.compositeapps.gia.models.Outlook.Recipient;
import com.compositeapps.gia.models.SalesForce.*;
import com.compositeapps.gia.services.retrofit.SalesForceServiceBuilder;
import com.compositeapps.gia.utils.MessageUtil;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class CasesService extends SFService {

    @Autowired
    private ContactService contactService;

    @Autowired
    private EmailService emailService;

    @Value("${sf.no-account-id}")
    private String noAccountId;

    @Value("${sf.case-record-type-id}")
    private String caseRecordTypeId;

    @Value("${sf.contact-record-type-id}")
    private String contactRecordTypeId;

    public Case getCaseDetails(String ID){

        try {
            Case response = SalesForceServiceBuilder.getSalesForceService(session.getSFToken(), session.getSFUrl()).findCaseById(ID).execute().body();
            if (response != null) {
                return response;
            }

            return null;

        } catch (IOException e) {
            return null;
        }

    }

    public String createCase(Message message) {
        ensureAccessToken();

        EmailAlias alias = MessageUtil.getMessageEmailAlias(message);
        String aliasName = "";

        try {
            Recipient fromEmailRecipient = message.getFrom();
            Recipient toEmailRecipient   = message.getToRecipients().get(0);

            String plainTextBody = Jsoup.parse(message.getBody().getContent()).text();

            Case request = new Case();
            request.setStatus("New");
            request.setPriority("Medium");
            request.setOrigin("Email");
            request.setReason("");
            request.setSubject(message.getSubject());
            request.setDescription(plainTextBody);
            request.setSuppliedEmail(fromEmailRecipient.getEmailAddress().getAddress());
            request.setSuppliedName(fromEmailRecipient.getEmailAddress().getName());
            request.setRecordTypeId(caseRecordTypeId);  // Set to 'Lab Client Service' record type

            if (alias == null) {
                String toAddress = toEmailRecipient.getEmailAddress().getAddress();
                request.setEmailAlias(toAddress);
                aliasName = toAddress;
            } else {
                request.setEmailAlias(alias.toString());
                aliasName = aliasName.toString();
            }

            request = addCaseContactInformation(request, message);

            // Create the case
            Response r = SalesForceServiceBuilder.getSalesForceService(session.getSFToken(), session.getSFUrl()).createCase(request).execute();
            SFCreateObjectResponse response = (SFCreateObjectResponse) r.body();
            if (r.code() == 201) {
                if (response.getSuccess()) {
                    return response.getId();
                } else {
                    printSFCreateObjectError(response, "Error(s) creating case");
                }

            } else {
                emailService.sendAlertEmail(
                        "Error Creating Case: " + message.getSubject(),
                        "Sent to: " + aliasName + "\n\nThere was an error creating the case for the message titled '" + message.getSubject() + "':\n " + r.code() + " - " + response.toString());
            }

        } catch (IOException e) {
            return null;
        }

        return null;
    }

    public Case getCaseByThreadId(String threadId) {
        ensureAccessToken();

        Case foundCase = null;
        try {
            String threadIdQuery = "select Id, CaseNumber, Thread_Identifier__c from Case where Thread_Identifier__c = '" + threadId + "'";
            SFQueryResponse<Case> caseQueryResponse = SalesForceServiceBuilder.getSalesForceService(session.getSFToken(), session.getSFUrl()).findCaseByThreadId(threadIdQuery).execute().body();
            if (caseQueryResponse != null && caseQueryResponse.getTotalSize() > 0) {
                Case[] records = caseQueryResponse.getRecords();
                foundCase      = records[0];
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return foundCase;
    }

    private Case addCaseContactInformation(Case request, Message message) {

        try {
            Recipient fromEmailRecipient = message.getFrom();

            // Check if email matches a current contact in system
            String contactQuery = "select id, accountId, email, description, account.id, account.name from contact where email='" + fromEmailRecipient.getEmailAddress().getAddress() + "'";
            SFQueryResponse<Contact> contactQueryResponse = SalesForceServiceBuilder.getSalesForceService(session.getSFToken(), session.getSFUrl()).getContactsByEmail(contactQuery).execute().body();
            if (contactQueryResponse != null && contactQueryResponse.getTotalSize() > 0) {
                List<Contact> contacts = new ArrayList<>();
                contacts.addAll(Arrays.asList(contactQueryResponse.getRecords()));

                if (contacts.size() > 0) {
                    Contact contact = contacts.get(0);
                    request.setContactId(contact.getId());

                    Account account = contact.getAccount();
                    if (account != null) {
                        request.setAccountId(account.getId());
                    }
                }
            }

            // Add case to account 'GIA Lab Services - No Account'
            else {
                request.setAccountId(noAccountId);

                String emailAddress = fromEmailRecipient.getEmailAddress().getAddress();

                Contact newContact = new Contact();
                newContact.setAccountId(noAccountId);
                newContact.setEmail(emailAddress);
                newContact.setLabContactType("Public");
                newContact.setRecordTypeId(contactRecordTypeId);    // Set record id to 'Lab Services' to enable lab contact type id to be set

                String fromName = fromEmailRecipient.getEmailAddress().getName();
                if (fromName != null && !fromName.isEmpty()) {
                    String[] names = fromName.split(" ");
                    if (names.length == 1 && names[0].length() > 0) {
                        newContact.setFirstName(names[0]);
                        newContact.setLastName(names[0]);
                    } else if (names.length == 2 && !names[0].isEmpty() && !names[1].isEmpty()) {
                        newContact.setFirstName(names[0]);
                        newContact.setLastName(names[1]);
                    } else if (names.length == 2 && !names[0].isEmpty()) {
                        newContact.setFirstName(names[0]);
                        newContact.setLastName("N/A");
                    } else if (names.length == 2) {
                        newContact.setFirstName(emailAddress);
                        newContact.setLastName("N/A");
                    }
                } else {
                    newContact.setFirstName(emailAddress);
                    newContact.setLastName(emailAddress);
                }

                if (newContact.getLastName() == null || newContact.getLastName().isEmpty()) {
                    newContact.setLastName("N/A");
                }

                String contactId = contactService.createContact(newContact);
                if (contactId != null && !contactId.isEmpty()) {
                    request.setAccountId(noAccountId);
                    request.setContactId(contactId);
                }

            }

        } catch (IOException e) {
            return request;
        }

        return request;

    }

}
