package com.compositeapps.gia.services.spring;

import com.compositeapps.gia.models.SalesForce.Contact;
import com.compositeapps.gia.models.SalesForce.SFCreateObjectResponse;
import com.compositeapps.gia.services.retrofit.SalesForceServiceBuilder;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class ContactService extends SFService {

    public String createContact(Contact request) {
        String tokens = session.getSFToken();
        if (tokens == null) {
            callGetToken();
        }

        try {

            // Create the case
            SFCreateObjectResponse response = SalesForceServiceBuilder.getSalesForceService(session.getSFToken(), session.getSFUrl()).createContact(request).execute().body();
            if (response != null) {
                if (response.getSuccess()) {
                    return response.getId();

                } else {
                    printSFCreateObjectError(response, "Error(s) creating contact");
                }
            }

        } catch (IOException e) {
            return null;
        }

        return null;

    }


}
