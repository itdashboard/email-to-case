package com.compositeapps.gia.services.spring;

import com.compositeapps.gia.entities.EmailLog;
import com.compositeapps.gia.enums.EmailAlias;
import com.compositeapps.gia.models.*;
import com.compositeapps.gia.models.Outlook.OutgoingMessage;
import com.compositeapps.gia.models.Outlook.OutgoingMessageContent;
import com.compositeapps.gia.models.Outlook.OutlookAttachment;
import com.compositeapps.gia.models.Outlook.Recipient;
import com.compositeapps.gia.models.SalesForce.*;
import com.compositeapps.gia.utils.MessageUtil;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class EmailListenerService implements DisposableBean  {

    private static final Logger LOGGER = Logger.getLogger(EmailListenerService.class.getName());

    private volatile boolean isRunning;
    private OutlookEmailService outlookEmailService;
    private CasesService casesService;
    private SFEmailService sfEmailService;
    private AttachmentService attachmentService;
    private EmailService emailService;
    private UserSession session;

    @Autowired
    EmailListenerService(OutlookEmailService outlookEmailService, CasesService casesService, AttachmentService attachmentService, SFEmailService sfEmailService, EmailService emailService) {
        this.isRunning = false;
        this.outlookEmailService = outlookEmailService;
        this.casesService = casesService;
        this.attachmentService = attachmentService;
        this.sfEmailService = sfEmailService;
        this.emailService = emailService;
        this.session = UserSession.getInstance();
    }

    @Override
    public void destroy(){
        isRunning = false;
    }

    @Async
    public void start() {
        if(!this.isRunning) {
            this.isRunning = true;
            while(isRunning){

                // fetch a new email message
                outlookEmailService.getUserMessage("inbox");  // fetches an email and updates the session

                // process inbox
                if (session.getInboxMessages().size() > 0) {
                    processMessageFromBox(UserSession.getInstance().getInboxMessages().get(0), "inbox");
                }

                try {
                    Thread.sleep(5000L);
                } catch (InterruptedException e) {
                    this.isRunning = false;
                    Thread.currentThread().interrupt();
                    LOGGER.log(Level.WARNING,"Interrupted Exception: " + e.getMessage(), e);
                    throw new RuntimeException(e);
                }

            }
        }
    }

    public Boolean isRunning() { return isRunning; }

    /**
     * Processes messages from the specified box
     * @param message The message
     * @param box The box name (i.e. "inbox")
     */
    private void processMessageFromBox(Message message, String box) {
        EmailLog log = new EmailLog();
        // Push message to SalesForce
        if (box.equalsIgnoreCase("inbox")) {
            if (isInAliasFolder(message)) {

                if (isEmailReply(message)) {
                    log = convertReplyToEmailMessage(message);
                } else if (isNewCase(message)) {
                    log = convertMessageToSalesForceCase(message);
                } else if (isGeneratedEmail(message)) {
                    log.setIsAutomated(true);
                    log.setIsReply(false);
                }  else {
                    log.setIsAutomated(false);
                    log.setIsReply(false);
                }

            } else {
                if (isEmailReply(message)) {
                    log = convertReplyToEmailMessage(message);
                } else {
                    log.setIsAutomated(false);
                    log.setIsReply(false);
                }
            }
        }

        outlookEmailService.logEmailMessage(log, message);

        outlookEmailService.markMessageAsRead(message, box);
    }

    /**
     * Determines whether the email message is an auto-generated message
     * @param message The message
     * @return True if generated, else false
     */
    private Boolean isGeneratedEmail(Message message) {
        return isAutomatedEmail(message) || isFromSalesForce(message) || isQuarantineEmail(message);
    }

    /**
     * Determines whether the email message is a reply
     * @param message The message
     * @return True if reply, else false
     */
    private Boolean isEmailReply(Message message) {
        return !isFromSalesForce(message) && !isAutomatedEmail(message) && !isQuarantineEmail(message) && hasThreadId(message);
    }

    /**
     * Determines whether the email message is a new case
     * @param message The message
     * @return True if reply, else false
     */
    private Boolean isNewCase(Message message) {
        return !isFromSalesForce(message) && !isQuarantineEmail(message) && !isAutomatedEmail(message);
    }

    /**
     * Determins whether the email is a quarantine email (i.e. from quarantine@gia.edu)
     * @param message The message
     * @return True if a quarantine email, else false
     */
    private Boolean isQuarantineEmail(Message message) {
        Recipient recipient = message.getFrom();
        if (recipient != null) {
            EmailAddress emailAddress = recipient.getEmailAddress();
            String address = emailAddress.getAddress();
            return address.equalsIgnoreCase("quarantine@gia.edu");
        }
        return false;
    }

    /**
     * Checks to see if the email is in an alias folder
     * @param message The messsage
     * @return True if in alias folder, else false
     */
    private Boolean isInAliasFolder(Message message) {
        EmailAlias emailAlias = MessageUtil.getMessageEmailAlias(message);
        if (emailAlias != EmailAlias.GIA_SALESFORCE) {
            return true;
        }
        return false;
    }

    /**
     * Determines if the message originated from SalesForce
     * @param message The message
     * @return True/False, is from SalesForce
     */
    private Boolean isFromSalesForce(Message message) {
        Boolean isFromSalesForce = false;
        // Check headers to see if sent from user in SalesForce org
        String sfEmailHeaderKey = "X-SFDC-LK";  // Header key that hold which SF org the email was sent from
        ArrayList<LinkedHashMap<String,String>> headers = new ArrayList<>();
        Object emailHeaders = message.getInternetMessageHeaders();
        if (emailHeaders instanceof ArrayList) {
            headers = (ArrayList<LinkedHashMap<String,String>>) emailHeaders;
            for (LinkedHashMap<String, String> header : headers) {

                String orgId     = null;
                Boolean getValue = false;
                for (String key : header.keySet()) {
                    if (key.equalsIgnoreCase("name")) {
                        String headerName = header.get(key);
                        if (headerName.equalsIgnoreCase(sfEmailHeaderKey)) {
                            getValue = true;
                        }
                    }

                    if (getValue && key.equalsIgnoreCase("value")) {
                        orgId = header.get(key);
                        break;
                    }
                }

                if (orgId != null) {
                    if (orgId.equalsIgnoreCase(session.getSF_PROD_ORG_ID()) || orgId.equalsIgnoreCase(session.getSF_SANDBOX_ORG_ID())) {
                        // Don't create a reply record in SF if the reply came from the SF Org
                        isFromSalesForce = true;
                        break;
                    }
                }
            }
        }
        return isFromSalesForce;
    }

    /**
     * Determines if the message is automated from Outlook
     * @param message The message
     * @return True/False, is automated
     */
    private Boolean isAutomatedEmail(Message message) {
        Boolean isAutomated = false;
        // Header key that holds auto-submitted / auto-generated info
        String automatedEmailKey = "Auto-Submitted";
        ArrayList<LinkedHashMap<String,String>> headers = new ArrayList<>();
        Object emailHeaders = message.getInternetMessageHeaders();
        if (emailHeaders instanceof ArrayList) {
            headers = (ArrayList<LinkedHashMap<String,String>>) emailHeaders;
            for (LinkedHashMap<String, String> header : headers) {

                for (String key : header.keySet()) {
                    if (key.equalsIgnoreCase("name")) {
                        String headerName = header.get(key);
                        if (headerName.equalsIgnoreCase(automatedEmailKey)) {
                            isAutomated = true;
                            break;
                        }
                    }
                }

            }
        }
        return isAutomated;
    }

    /**
     * Determines if the message is a valid email meaning:
     * 1) Is not an alert or message from SalesForce,
     * 2) Is not an automated message
     * @param message The message
     * @return True/False, is valid
     */
    private Boolean isValidEmail(Message message) {
        Boolean isValid = true;

        // Check headers to see if sent from user in SalesForce org or if automated
        String sfEmailHeaderKey  = "X-SFDC-LK";         // Header key that holds which SF org the email was sent from
        String automatedEmailKey = "Auto-Submitted";    // Header key that holds auto-submitted / auto-generated info

        ArrayList<LinkedHashMap<String,String>> headers = new ArrayList<>();
        Object emailHeaders = message.getInternetMessageHeaders();
        if (emailHeaders instanceof ArrayList) {
            headers = (ArrayList<LinkedHashMap<String,String>>) emailHeaders;
            for (LinkedHashMap<String, String> header : headers) {

                String orgId            = null;
                Boolean isAutoSubmitted = false;
                Boolean isFromSF        = false;
                Boolean getValue        = false;

                for (String key : header.keySet()) {
                    if (key.equalsIgnoreCase("name")) {
                        String headerName = header.get(key);
                        if (headerName.equalsIgnoreCase(sfEmailHeaderKey)) {
                            getValue = true;
                        }

                        if (headerName.equalsIgnoreCase(automatedEmailKey)) {
                            isAutoSubmitted = true;
                        }
                    }

                    if (getValue && key.equalsIgnoreCase("value")) {
                        orgId = header.get(key);
                    }
                }

                if (orgId != null) {
                    if (orgId.equalsIgnoreCase(session.getSF_PROD_ORG_ID()) || orgId.equalsIgnoreCase(session.getSF_SANDBOX_ORG_ID())) {
                        // Don't create a reply record in SF if the reply came from the SF Org
                        isFromSF = true;
                        break;
                    }
                }

                LOGGER.info("Email is from SalesForce = " + isFromSF);
                LOGGER.info("Email is automated = " + isAutoSubmitted);

                isValid = (!isFromSF && !isAutoSubmitted);

            }
        }

        LOGGER.info("Email is valid = " + isValid);

        return isValid;
    }

    /**
     * Checks if the message has a thread ID within the body
     * @param message The message
     * @return True if has a thread id, else false
     */
    private Boolean hasThreadId(Message message) {
        Boolean hasThreadId = false;

        String plainBody = Jsoup.parse(message.getBody().getContent()).text();

        Pattern threadIdRegex   = Pattern.compile("ref:(.*):ref");
        Matcher threadIdMatcher = threadIdRegex.matcher(plainBody);
        hasThreadId             = threadIdMatcher.find();

        return hasThreadId;
    }

    /**
     * Creates the message as a reply in SalesForce
     * @param message The message
     * @return The messages' email log instance
     */
    private EmailLog convertReplyToEmailMessage(Message message) {
        EmailLog log = new EmailLog();
        log.setIsReply(true);
        log.setIsAutomated(false);

        String plainBody = Jsoup.parse(message.getBody().getContent()).text();

        // Grab thread id
        Pattern threadIdRegex   = Pattern.compile("(ref:)(_[\\d\\w]*\\._[\\d\\w]*)(:ref)");
        Matcher threadIdMatcher = threadIdRegex.matcher(plainBody);
        Boolean hasThreadId     = threadIdMatcher.find();

        if (hasThreadId) {
            String threadId = threadIdMatcher.group(0);
            String caseId   = null;

            if (threadId != null) {
                log.setThreadId(threadId);

                Case existingCase = casesService.getCaseByThreadId(threadId);
                if (existingCase != null) {
                    caseId = existingCase.getId();

                    log.setCaseId(caseId);
                    log.setCaseNo(existingCase.getCaseNumber());

                    String emailMessageId = sfEmailService.createCaseEmailMessage(message, caseId, threadId);
                    if (emailMessageId != null) {
                        log.setThreadId(threadId);
                        LOGGER.log(Level.INFO, "Created email message reply " + emailMessageId);
                    }

                    Boolean hasAttachments = message.getHasAttachments();
                    if (hasAttachments != null && hasAttachments) {
                        log = convertAttachmentsToSFAttachments(message, log, emailMessageId);
                    } else {
                        log.setTotalAttachments(0);
                    }

                } else {
                    emailService.sendAlertEmail(
                            "Error Creating Reply: " + message.getSubject(),
                            "Thread ID: " + threadId + "\n\n An email message reply record for the message titled '" + message.getSubject() + "' was not created. There was an error or existing case was not found. ");
                }

            }

        }
        return log;
    }

    /**
     * Creates a case for the email message
     * @param message The message
     * @return The messages' email log instance
     */
    private EmailLog convertMessageToSalesForceCase(Message message) {
        EmailLog log = new EmailLog();
        log.setIsReply(false);
        log.setIsAutomated(false);

        String caseId = casesService.createCase(message);
        if (caseId != null) {
            LOGGER.log(Level.INFO, "Successfully converted email into case with ID #" + caseId);

            Case caseInfo = casesService.getCaseDetails(caseId);
            if (caseInfo != null) {
                String caseNo = caseInfo.getCaseNumber();
                log.setCaseId(caseId);
                log.setCaseNo(caseNo);

                String threadId = generateSFThreadId(caseId);
                String emailMessageId = sfEmailService.createCaseEmailMessage(message, caseId, threadId);
                if (emailMessageId != null) {
                    log.setThreadId(threadId);
                    LOGGER.log(Level.INFO, "Created email message " + emailMessageId + " for case with ID #" + caseId);
                }

                Boolean hasAttachments = message.getHasAttachments();
                if (hasAttachments != null && hasAttachments) {
                    log = convertAttachmentsToSFAttachments(message, log, emailMessageId);
                } else {
                    log.setTotalAttachments(0);
                }

            }
        }
        return log;
    }

    /**
     * Creates the SalesForce thread ID
     * @param caseId The case ID
     * @return The thread ID
     */
    private String generateSFThreadId(String caseId) {
        String threadId = "";

        String organizationId = session.getSFOrganizationId();
        if (organizationId == null || organizationId.isEmpty()) {
            sfEmailService.getUserInfo();
            organizationId = session.getSFOrganizationId();
        }

        if (organizationId != null) {
            String shortenedOrgId  = getShortenedOrgId(organizationId);
            String shortenedCaseId = getShortenedCaseId(caseId);
            threadId = "ref:_" + shortenedOrgId + "._" + shortenedCaseId + ":ref";
        }

        return threadId;
    }

    /**
     * Generates the shortened organization id using SalesForce's current logic
     * Will need to be updated again when / if SalesForce decides to update how they generate their thread ids
     * @param id The id string
     * @return The shortened id
     */
    private String getShortenedOrgId(String id) {
        String firstPart  = id.substring(0, 5);                 // Get first 5 characters of org id
        String splicedId  = id.substring(0, id.length() - 3);   // Remove last 3 characters of org id
        String secondPart = splicedId.substring(splicedId.length() - 4, splicedId.length()); // Get last 4 characters of org id after removing the last 3
        return firstPart + secondPart;
    }

    /**
     * Generates the shortened case id using SalesForce's current logic
     * Will need to be updated again when / if SalesForce decides to update how they generate their thread ids
     * @param id The id string
     * @return The shortened id
     */
    private String getShortenedCaseId(String id) {
        String firstPart  = id.substring(0, 5);                 // Get first 5 characters of case id
        String splicedId  = id.substring(0, id.length() - 3);   // Remove last 3 characters of case id
        String secondPart = splicedId.substring(splicedId.length() - 5, splicedId.length()); // Get last 5 characters of case id after removing the last 3
        return firstPart + secondPart;
    }

    /**
     * Adds all attachments within an email into it's associated case in SalesForce
     * @param message The message
     * @param emailMessageId The email message id
     */
    private EmailLog convertAttachmentsToSFAttachments(Message message, EmailLog emailLog, String emailMessageId) {
        OutlookAttachment[] attachments = outlookEmailService.getMessageAttachments(message.getId());
        if (attachments != null && attachments.length > 0) {

            emailLog.setTotalAttachments(attachments.length);

            for (OutlookAttachment attachment : attachments) {
                SFAttachment sfAttachment = new SFAttachment();
                sfAttachment.setParentId(emailMessageId);
                sfAttachment.setName(attachment.getName());
                sfAttachment.setContentType(attachment.getContentType());
                sfAttachment.setBody(attachment.getContentBytes());
                attachmentService.createAttachment(sfAttachment);
            }

        }

        return emailLog;

    }

    /**
     * Sends an automated reply to the message sender
     * @param message The message
     * @param caseNo The case number
     */
    private void sendAutomatedReply(Message message, String caseNo) {
        MessageBodyContent content = new MessageBodyContent();
        content.setContentType("HTML");
        content.setContent("Thank you for your email. This is your case number: " + caseNo + ". Your case has been " +
                "logged and will be reviewed by your Customer Service Representative. We will get back to you as " +
                "soon as possible. <br><br> Please do not reply directly to this automated email.");

        OutgoingMessageContent replyMsg = new OutgoingMessageContent();
        replyMsg.setSubject("Case #" + caseNo);
        replyMsg.setBody(content);
        replyMsg.setToRecipients(new Recipient[] { message.getFrom() });
        replyMsg.setCcRecipients(new Recipient[] {});

        OutgoingMessage outgoingMessage = new OutgoingMessage();
        outgoingMessage.setMessage(replyMsg);

        outlookEmailService.sendMessage(outgoingMessage);
    }

}
