package com.compositeapps.gia.services.spring;

import com.compositeapps.gia.models.OutboundEmailRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class EmailService {

    @Value("${spring.profiles.active}")
    private String activeProfile;

    private final Logger LOGGER = Logger.getLogger(EmailService.class.getName());

    private final String[] ALERT_EMAILS = new String[] { "jasmine.fortich@compositeapps.net" };

    private final String HOST  = "smtp.office365.com";
    private final int PORT     = 587;
    private final String EMAIL = "giasalesforce@gia.edu";
    private final String PW    = "s2RJcUFbpZ7cg";

    /**
     * Sends an outbound email
     * @param messageRequest The message request
     * @throws MessagingException Thrown if error with the email
     */
    public void sendEmail(OutboundEmailRequest messageRequest) throws MessagingException {
        final Session session = Session.getInstance(this.getEmailProperties(), new Authenticator() {

            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(EMAIL, PW);
            }

        });

        final Message message = new MimeMessage(session);

        for (String toRecipient : messageRequest.getToRecipients()) {
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(toRecipient));
        }

        for (String ccRecipient : messageRequest.getBccRecipients()) {
            message.setRecipient(Message.RecipientType.BCC, new InternetAddress(ccRecipient));
        }

        message.setSubject(messageRequest.getSubject());
        message.setContent(messageRequest.getHtmlBody(), "text/html; charset=utf-8");
        message.setFrom(new InternetAddress(messageRequest.getFromAddress()));
        message.setSentDate(new Date());

        Transport.send(message);
    }

    /**
     * Sends an informational alert email to the static xalert email addresses listed
     * @param subject The subject line
     * @param body The email body
     */
    public void sendAlertEmail(String subject, String body) {
        try {

            final Session session = Session.getInstance(this.getEmailProperties(), new Authenticator() {

                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(EMAIL, PW);
                }

            });

            for (String email : ALERT_EMAILS) {
                final Message message = new MimeMessage(session);
                message.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
                message.setFrom(new InternetAddress(EMAIL));
                message.setSubject(subject);
                message.setText(body);
                message.setSentDate(new Date());
                Transport.send(message);
            }

        } catch (MessagingException e) {
            LOGGER.log(Level.WARNING, "Error sending message: " + e.getMessage(), e);
        }
    }

    public Properties getEmailProperties() {
        final Properties config = new Properties();
        config.put("mail.transport.protocol", "smtp");
        config.put("mail.smtp.auth", "true");
        config.put("mail.smtp.starttls.enable", "true");
        config.put("mail.smtp.host", HOST);
        config.put("mail.smtp.port", PORT);
        return config;
    }

}
