package com.compositeapps.gia.services.spring;

import com.compositeapps.gia.entities.EmailLog;
import com.compositeapps.gia.enums.EmailAlias;
import com.compositeapps.gia.models.SalesForce.MailFolder;
import com.compositeapps.gia.models.SalesForce.Message;
import com.compositeapps.gia.models.Outlook.OutgoingMessage;
import com.compositeapps.gia.models.Outlook.OutlookAttachment;
import com.compositeapps.gia.models.Outlook.PagedResult;
import com.compositeapps.gia.models.Outlook.TokenResponse;
import com.compositeapps.gia.models.UserSession;
import com.compositeapps.gia.repositories.EmailLogRepository;
import com.compositeapps.gia.models.*;
import com.compositeapps.gia.services.retrofit.OutlookService;
import com.compositeapps.gia.services.retrofit.OutlookServiceBuilder;
import com.compositeapps.gia.utils.AuthHelper;
import com.compositeapps.gia.utils.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class OutlookEmailService {

    private UserSession session = UserSession.getInstance();
    private EmailLogRepository emailLogRepository;

    private final String INBOX_FOLDER   = "inbox";
    private final String SENT_FOLDER    = "send";

    @Autowired
    public OutlookEmailService(EmailLogRepository emailLogRepository) {
        this.emailLogRepository = emailLogRepository;
    }

    public void getUserMessage(String folder) {
        UserSession userSession = UserSession.getInstance();

        TokenResponse tokens = userSession.getTokenResponse();//(TokenResponse)session.getAttribute("tokens");
        if (tokens == null) {
            return;
        }

        // Sort by time received in descending order
        String sort = "receivedDateTime DESC";
        // Only return the properties we care about
        String properties = "receivedDateTime,from,ToRecipients,isRead,subject,bodyPreview,InternetMessageHeaders,HasAttachments,Body,ParentFolderId";
        // Filter for unread messages
        String filter = "isRead eq false";
        // Return at most 10 messages
        Integer maxResults = 1;
        String tenantId = userSession.getTenantId();
        String email    = userSession.getUserEmail();

        tokens = AuthHelper.ensureTokens(tokens, tenantId);
        session.setTokenResponse(tokens);

        OutlookService outlookService = OutlookServiceBuilder.getOutlookService(tokens.getAccessToken(), email);

        try {

            PagedResult<Message> messages = outlookService.getMessages(
                    sort, properties, filter, maxResults)
                    .execute().body();
            if (messages != null && messages.getValue() != null && messages.getValue().length > 0) {
                addMessageToFolder(folder, messages.getValue()[0]);
                UserSession.getInstance().getMessageHistory().add(messages.getValue()[0]);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public OutlookAttachment[] getMessageAttachments(String messageId) {
        OutlookService outlookService = OutlookServiceBuilder.getOutlookService(
                session.getTokenResponse().getAccessToken(), session.getUserEmail());
        try {

            PagedResult<OutlookAttachment> attachments = outlookService.getMessageAttachments(messageId).execute().body();
            if (attachments != null && attachments.getValue().length > 0) {
                return attachments.getValue();
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        return null;
    }

    public Boolean sendMessage(OutgoingMessage message) {
        Boolean isSent = false;
        OutlookService outlookService = OutlookServiceBuilder.getOutlookService(
                session.getTokenResponse().getAccessToken(), session.getUserEmail());

        try {

            Response response = outlookService.sendMessage(message).execute();
            if (response.code() == 202) {
                isSent = true;
            }

        } catch (Exception e) {
            e.getMessage();
        }

        return isSent;

    }

    public Boolean deleteMessage(Message message, String box) {
        Boolean isDeleted = false;

        OutlookService outlookService = OutlookServiceBuilder.getOutlookService(
                session.getTokenResponse().getAccessToken(), session.getUserEmail());
        try {
            Response r =  outlookService.deleteMessage(message.getId()).execute();
            if(r.code() != 403) {
                // handle
                isDeleted = true;
                getMessageFolder(box).remove(message);
            }

        } catch(Exception e) {
            e.getMessage();
        }

        return isDeleted;
    }

    private List<Message> getMessageFolder(String folder) {
        List<Message> messages = null;
        switch(folder) {
            case INBOX_FOLDER:
                messages = session.getInboxMessages();
                break;
            case SENT_FOLDER:
                messages = session.getSentMessages();
                break;
            default:
                messages = new ArrayList<>();
        }

        return messages;
    }

    private void addMessageToFolder(String folder, Message message) {
        switch(folder) {
            case INBOX_FOLDER:
                session.addInboxMessage(message);
                break;
            case SENT_FOLDER:
                session.addSentMessage(message);
                break;
        }
    }

    public void logEmailMessage(Message message) {
        EmailLog log = new EmailLog();

        LocalDateTime sent = Instant.ofEpochMilli(message.getReceivedDateTime().getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();

        log.setBody(message.getBody().getContent());
        log.setFrom(message.getFrom().getEmailAddress().getAddress());
        log.setRecipients(message.getToRecipients().toString());
        log.setSubject(message.getSubject());
        log.setHeader(message.getInternetMessageHeaders().toString());
        log.setIsRead(true);
        log.setDateSent(sent);

        emailLogRepository.save(log);
    }

    public void logEmailMessage(EmailLog log, Message message) {
        try {

            // Get Email Alias
            EmailAlias alias = MessageUtil.getMessageEmailAlias(message);
            String aliasName = alias != null ? alias.toString() : "";

            // Get UTC timestamp
            LocalDateTime sent = Instant.ofEpochMilli(message.getReceivedDateTime().getTime())
                    .atZone(ZoneId.of("UTC"))
                    .toLocalDateTime();

            // Get sender IP address and format headers to unicode
            ArrayList<HashMap<String, String>> headers = (ArrayList<HashMap<String, String>>) message.getInternetMessageHeaders();
            String senderIp = getSenderIpAddress(headers);
            String unicodeHeaders = convertHeadersToUnicodeFormat(headers);

            log.setMessageId(message.getId());
            log.setSenderIp(senderIp);
            log.setBody(message.getBody().getContent());
            log.setEmailAlias(aliasName);
            log.setFrom(message.getFrom().getEmailAddress().getAddress());
            log.setRecipients(message.getToRecipients().toString());
            log.setSubject(message.getSubject());
            log.setHeader(unicodeHeaders);
            log.setIsRead(true);
            log.setDateSent(sent);

            emailLogRepository.save(log);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<EmailLog> getEmailLogsByThreadId(String threadId) {
        List<EmailLog> logs = new ArrayList<>();
        try {
            logs = emailLogRepository.findByThreadIdOrderByDateSentDesc(threadId);
            return logs;
        } catch (Exception e) {
            return logs;
        }
    }

    public Boolean markMessageAsRead(Message message, String box) {
        Boolean isMarkedRead = false;

        OutlookService outlookService = OutlookServiceBuilder.getOutlookService(
                session.getTokenResponse().getAccessToken(), session.getUserEmail());
        try {
            Message updatedMessage = new Message();
            updatedMessage.setIsRead(true);

            Response r =  outlookService.updateMessage(message.getId(), updatedMessage).execute();
            if (r.code() == 200) {
                getMessageFolder(box).remove(message);
                isMarkedRead = true;
            }

        } catch(Exception e) {
            e.printStackTrace();
        }

        return isMarkedRead;
    }

    public Boolean moveMessageToFolder(String messageId, String folderId) {
        Boolean isMoved = false;

        OutlookService outlookService = OutlookServiceBuilder.getOutlookService(
                session.getTokenResponse().getAccessToken(), session.getUserEmail());
        try {
            MoveMessageRequest request = new MoveMessageRequest();
            request.setDestinationId(folderId);

            Message message = outlookService.moveMessage(messageId, request).execute().body();
            if (message != null) {
                isMoved = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return isMoved;
    }

    public Message moveMessageToAliasFolder(Message message) {
        OutlookService outlookService = OutlookServiceBuilder.getOutlookService(
                session.getTokenResponse().getAccessToken(), session.getUserEmail());
        try {

            PagedResult<MailFolder> folders = outlookService.getAllMailFolders().execute().body();

            EmailAlias usedAlias = null;
            String emailAlias    = message.getFrom().getEmailAddress().getAddress();

            if (emailAlias.equalsIgnoreCase(EmailAlias.ANTWERP.toString())) {
                usedAlias = EmailAlias.ANTWERP;
            } else if (emailAlias.equalsIgnoreCase(EmailAlias.BANGKOK.toString())) {
                usedAlias = EmailAlias.BANGKOK;
            } else if (emailAlias.equalsIgnoreCase(EmailAlias.BOTSWANA.toString())) {
                usedAlias = EmailAlias.BOTSWANA;
            } else if (emailAlias.equalsIgnoreCase(EmailAlias.CARLSBAD.toString())) {
                usedAlias = EmailAlias.CARLSBAD;
            } else if (emailAlias.equalsIgnoreCase(EmailAlias.DUBAI.toString())) {
                usedAlias = EmailAlias.DUBAI;
            } else if (emailAlias.equalsIgnoreCase(EmailAlias.HONG_KONG.toString())) {
                usedAlias = EmailAlias.HONG_KONG;
            } else if (emailAlias.equalsIgnoreCase(EmailAlias.MUMBAI.toString())) {
                usedAlias = EmailAlias.MUMBAI;
            } else if (emailAlias.equalsIgnoreCase(EmailAlias.NEW_YORK.toString())) {
                usedAlias = EmailAlias.NEW_YORK;
            } else if (emailAlias.equalsIgnoreCase(EmailAlias.RAMAT_GAN.toString())) {
                usedAlias = EmailAlias.RAMAT_GAN;
            } else if (emailAlias.equalsIgnoreCase(EmailAlias.SOUTH_AFRICA.toString())) {
                usedAlias = EmailAlias.SOUTH_AFRICA;
            } else if (emailAlias.equalsIgnoreCase(EmailAlias.SURAT.toString())) {
                usedAlias = EmailAlias.SURAT;
            } else if (emailAlias.equalsIgnoreCase(EmailAlias.TOKYO.toString())) {
                usedAlias = EmailAlias.TOKYO;
            }

            if (usedAlias != null) {
                String folderId = getAliasFolderId(usedAlias, folders);
                if (folderId != null) {
                    MoveMessageRequest request = new MoveMessageRequest();
                    request.setDestinationId(folderId);

                    message = outlookService.moveMessage(message.getId(), request).execute().body();

                }
            }

        } catch(Exception e) {
            e.getMessage();
        }

        return message;
    }

    private String getSenderIpAddress(ArrayList<HashMap<String, String>> headers) {
        String authenticationHeader = null;
        for (HashMap<String, String> header : headers) {
            String headerName = header.get("name");
            if (headerName.equalsIgnoreCase("Authentication-Results")) {
                authenticationHeader = header.get("value");
                break;
            } else if (headerName.equalsIgnoreCase("X-Originating-IP")) {
                authenticationHeader = header.get("value");
                break;
            }
        }

        if (authenticationHeader != null) {
            Pattern ipRegex = Pattern.compile("sender IP is (\\d+\\.\\d+\\.\\d+\\.\\d+)");
            Matcher ipMatcher = ipRegex.matcher(authenticationHeader);
            if (ipMatcher.find()) {
                if (ipMatcher.groupCount() > 0) {
                    return ipMatcher.group(1);
                }
            } else {
                ipRegex = Pattern.compile("(\\d+\\.\\d+\\.\\d+\\.\\d+)");
                ipMatcher = ipRegex.matcher(authenticationHeader);
                if (ipMatcher.find()) {
                    if (ipMatcher.groupCount() > 0) {
                        return ipMatcher.group(1);
                    }
                }
            }
        }

        return "";
    }

    private String convertHeadersToUnicodeFormat(ArrayList<HashMap<String, String>> headers) {
        StringBuilder headerBuilder = new StringBuilder();
        for (HashMap<String, String> header : headers) {
            String headerName  = header.get("name");
            String headerValue = header.get("value");

            headerBuilder.append(headerName);
            headerBuilder.append(": ");
            headerBuilder.append(headerValue);
            headerBuilder.append("\n");

        }
        return headerBuilder.toString();
    }

    private String getAliasFolderId(EmailAlias alias, PagedResult<MailFolder> folders) {
        String folderId = null;

        OutlookService outlookService = OutlookServiceBuilder.getOutlookService(
                session.getTokenResponse().getAccessToken(), session.getUserEmail());

        try {
            if (folders != null) {
                // Loop through folders to find alias folder
                for (MailFolder folder : folders.getValue()) {
                    if (folder.getDisplayName().equalsIgnoreCase(alias.toString())) {
                        folderId = folder.getId();
                        break;
                    }
                }

                // Create folder for the alias if not found
                if (folderId == null) {
                    MailFolder request = new MailFolder();
                    request.setDisplayName(alias.toString());

                    MailFolder newFolder = outlookService.createFolder(request).execute().body();
                    if (newFolder != null) {
                        folderId = newFolder.getId();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return folderId;
    }

}
