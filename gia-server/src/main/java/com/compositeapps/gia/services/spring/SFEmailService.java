package com.compositeapps.gia.services.spring;

import com.compositeapps.gia.enums.EmailAlias;
import com.compositeapps.gia.models.Outlook.Recipient;
import com.compositeapps.gia.models.SalesForce.Message;
import com.compositeapps.gia.models.SalesForce.SFCreateObjectResponse;
import com.compositeapps.gia.models.SalesForce.SFEmailMessage;
import com.compositeapps.gia.services.retrofit.SalesForceServiceBuilder;
import com.compositeapps.gia.utils.MessageUtil;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.IOException;
import java.text.SimpleDateFormat;

@Service
public class SFEmailService extends SFService {

    @Autowired
    private EmailService emailService;

    public String createCaseEmailMessage(Message message, String caseId, String threadId) {

        try {
            Recipient fromRecipient = message.getFrom();
            Recipient toRecipient = message.getToRecipients().get(0);

            EmailAlias alias = MessageUtil.getMessageEmailAlias(message);
            String aliasName = toRecipient.getEmailAddress().getAddress();

            String subject  = message.getSubject();
            String htmlBody = message.getBody().getContent();
            String plainTextBody = Jsoup.parse(htmlBody).text();

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            String messageDate = df.format(message.getReceivedDateTime());

            SFEmailMessage request = new SFEmailMessage();
            request.setStatus("0");
            request.setHeaders(message.getInternetMessageHeaders().toString());
            request.setFromName(fromRecipient.getEmailAddress().getName());
            request.setFromAddress(fromRecipient.getEmailAddress().getAddress());
            request.setSubject(subject);
            request.setHtmlBody(htmlBody);
            request.setTextBody(plainTextBody);
            request.setMessageDate(messageDate);
            request.setParentId(caseId);
            request.setThreadIdentifier(threadId);
            request.setIncoming(true);

            if (alias != null) {
                aliasName = alias.toString();
            }
            request.setToAddress(aliasName);

            Response r = SalesForceServiceBuilder.getSalesForceService(session.getSFToken(), session.getSFUrl()).createEmailMessage(request).execute();
            SFCreateObjectResponse response = (SFCreateObjectResponse) r.body();
            if (response != null) {
                if (response.getSuccess()) {
                    return response.getId();
                } else {
                    emailService.sendAlertEmail(
                            "Error Creating Email Message: " + message.getSubject(),
                            "Sent to: " + aliasName + "\n\nThere was an error creating an email message for the message titled '" + message.getSubject() + "':\n " + r.code() + " - " + response.toString());
                }
            }

        } catch (IOException e) {
            return null;
        }

        return null;
    }

}
