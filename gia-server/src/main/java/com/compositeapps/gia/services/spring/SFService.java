package com.compositeapps.gia.services.spring;

import com.compositeapps.gia.models.*;
import com.compositeapps.gia.models.Outlook.UserInfo;
import com.compositeapps.gia.models.SalesForce.SFCreateObjectResponse;
import com.compositeapps.gia.models.SalesForce.SFError;
import com.compositeapps.gia.models.SalesForce.SFTokenResponse;
import com.compositeapps.gia.services.retrofit.SalesForceServiceBuilder;
import com.compositeapps.gia.utils.EncryptUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SFService {

    public UserSession session = UserSession.getInstance();

    private static final Logger LOGGER = Logger.getLogger(SFService.class.getName());

    private static Boolean SENT_GET_TOKEN_NOTIFICATION = false;

    @Value("${spring.profiles.active}")   private String activeProfile;
    @Value("${sf.auth-url}") private String sfAuthUrl;
    @Value("${sf.username}") private String username;
    private String clientId;
    private String clientSecret;
    private String password;

    @Autowired
    private EmailService emailService;

    @PostConstruct
    public void init() throws IOException {
        this.clientId     = EncryptUtil.getEncryptedVaue(activeProfile, "sf.client-id");
        this.clientSecret = EncryptUtil.getEncryptedVaue(activeProfile, "sf.client-secret");
        this.password     = EncryptUtil.getEncryptedVaue(activeProfile, "sf.password");
    }

    /**
     * Calls get token to retrieve access token and tests the token with getUserInfo() call
     */
    public void ensureAccessToken() {
        callGetToken();
        getUserInfo();
    }

    /**
     * Retrieves an access token from SalesForce
     */
    public void callGetToken(){
        try {
            SFTokenResponse response = SalesForceServiceBuilder.getSalesForceService("", sfAuthUrl).authenticateUser("password", clientId, clientSecret, username, password).execute().body();
            if (response != null) {
                session.setSFToken(response.getAccessToken());
                session.setSFUrl(response.getInstanceUrl());
            } else {
                if (!SENT_GET_TOKEN_NOTIFICATION) {
                    emailService.sendAlertEmail(
                            "SalesForce Token Response Null",
                            "The SalesForce Token Response is returning null: " + response.toString());
                    SENT_GET_TOKEN_NOTIFICATION = true;
                }
            }

        } catch (IOException e) {
            return;
        }
    }

    /**
     * Retrieves user information from SalesForce
     */
    public void getUserInfo() {
        try {
            UserInfo userInfo = SalesForceServiceBuilder.getSalesForceService(session.getSFToken(), session.getSFUrl()).getCurrentUserInfo().execute().body();
            if (userInfo != null) {
                session.setSFOrganizationId(userInfo.getOrganizationId());
            }
        } catch (IOException e) {
            return;
        }
    }

    /**
     * Prints the create object error response
     * @param response The response
     * @param errorMsg The error message
     */
    public void printSFCreateObjectError(SFCreateObjectResponse response, String errorMsg) {
        SFError[] errors = response.getErrors();
        if (errors.length > 0) {
            errorMsg = errorMsg + ": ";
            for (SFError error : errors) {
                errorMsg += "[" + error.getErrorCode() + "] : " + error.getMessage() + ", ";
            }
            errorMsg = errorMsg.substring(0, errorMsg.lastIndexOf(","));
            LOGGER.log(Level.WARNING, errorMsg);
        }
    }

}
