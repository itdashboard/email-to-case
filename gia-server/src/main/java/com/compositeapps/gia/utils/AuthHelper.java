package com.compositeapps.gia.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.compositeapps.gia.models.Outlook.TokenResponse;
import com.compositeapps.gia.services.retrofit.TokenService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import org.springframework.web.util.UriComponentsBuilder;

@Component
public class AuthHelper {
    private static final String authority    = "https://login.microsoftonline.com";
    private static final String authorizeUrl = authority + "/common/oauth2/v2.0/authorize";
    private static final Logger LOGGER       = Logger.getLogger(AuthHelper.class.getName());

    private static String appId;
    private static String appPassword;
    private static String redirectUrl;
    private static String profile;

    private static String[] scopes = {
            "openid",
            "offline_access",
            "profile",
            "User.ReadWrite",
            "Mail.ReadWrite",
            "Mail.Send",
            "Mail.Send.Shared"
    };

    public void setAppId(String id) {
        appId = id;
    }

    public void setAppPassword(String password) {
        appPassword = password;
    }

    @Value("${redirect-url}")
    public void setRedirectUrl(String url) {
        redirectUrl = url;
    }

    @Value("${spring.profiles.active}")
    public void setProfile(String activeProfile) {
        profile = activeProfile;
    }

    private static String getAppId() {
        if (appId == null) {
            try {
                loadConfig();
            } catch (Exception e) {
                return null;
            }
        }
        return appId;
    }
    private static String getAppPassword() {
        if (appPassword == null) {
            try {
                loadConfig();
            } catch (Exception e) {
                return null;
            }
        }
        return appPassword;
    }

    private static String getRedirectUrl() {
        if (redirectUrl == null) {
            try {
                loadConfig();
            } catch (Exception e) {
                return null;
            }
        }
        return redirectUrl;
    }

    private static String getScopes() {
        StringBuilder sb = new StringBuilder();
        for (String scope: scopes) {
            sb.append(scope + " ");
        }
        return sb.toString().trim();
    }

    private static void loadConfig() throws IOException {

        String authConfigFile = "application-" + profile + ".properties";
        InputStream authConfigStream = AuthHelper.class.getClassLoader().getResourceAsStream(authConfigFile);

        if (authConfigStream != null) {
            Properties authProps = new Properties();
            try {

                authProps.load(authConfigStream);
                appId = EncryptUtil.getEncryptedVaue(profile, "app-id");
                appPassword = EncryptUtil.getEncryptedVaue(profile, "app-password");
                redirectUrl = authProps.getProperty("redirect-url");

                LOGGER.log(Level.INFO, "Using properties file: " + authConfigFile);
                LOGGER.log(Level.INFO, "Application Id: " + appId);
                LOGGER.log(Level.INFO, "Redirect URL: " + redirectUrl);

            } finally {
                authConfigStream.close();
            }
        }
        else {
            throw new FileNotFoundException("Property file '" + authConfigFile + "' not found in the classpath.");
        }
    }

    public static String getLoginUrl(UUID state, UUID nonce) {

        UriComponentsBuilder urlBuilder = UriComponentsBuilder.fromHttpUrl(authorizeUrl);
        urlBuilder.queryParam("client_id", getAppId());
        urlBuilder.queryParam("redirect_uri", getRedirectUrl());
        urlBuilder.queryParam("response_type", "code id_token");
        urlBuilder.queryParam("scope", getScopes());
        urlBuilder.queryParam("state", state);
        urlBuilder.queryParam("nonce", nonce);
        urlBuilder.queryParam("response_mode", "form_post");

        return urlBuilder.toUriString();
    }

    public static TokenResponse getTokenFromAuthCode(String authCode, String tenantId) {
        // Create a logging interceptor to log request and responses
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor).build();

        // Create and configure the Retrofit object
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(authority)
                .client(client)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        // Generate the token service
        TokenService tokenService = retrofit.create(TokenService.class);

        try {
            return tokenService.getAccessTokenFromAuthCode(tenantId, getAppId(), getAppPassword(),
                    "authorization_code", authCode, getRedirectUrl()).execute().body();
        } catch (IOException e) {
            TokenResponse error = new TokenResponse();
            error.setError("IOException");
            error.setErrorDescription(e.getMessage());
            return error;
        }
    }

    public static TokenResponse ensureTokens(TokenResponse tokens, String tenantId) {
        // Are tokens still valid?

        // Note: This calculation is incorrect as tokens.getExpirationTime() is in seconds not epoch; Commented out for now to refresh tokens constantly
//        Calendar now = Calendar.getInstance();
//        if (!now.getTime().before(tokens.getExpirationTime())) {
//            // Still valid, return them as-is
//            return tokens;
//        }
//        else {
            LOGGER.log(Level.INFO, "Refreshing tokens..");

            // Expired, refresh the tokens
            // Create a logging interceptor to log request and responses
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor).build();

            // Create and configure the Retrofit object
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(authority)
                    .client(client)
                    .addConverterFactory(JacksonConverterFactory.create())
                    .build();

            // Generate the token service
            TokenService tokenService = retrofit.create(TokenService.class);

            try {
                return tokenService.getAccessTokenFromRefreshToken(tenantId, getAppId(), getAppPassword(),
                        "refresh_token", tokens.getRefreshToken(), getRedirectUrl()).execute().body();
            } catch (IOException e) {
                LOGGER.log(Level.WARNING, "Error refreshing tokens: " + e.getMessage(), e);
                TokenResponse error = new TokenResponse();
                error.setError("IOException");
                error.setErrorDescription(e.getMessage());
                return error;
            }
//        }
    }
}