package com.compositeapps.gia.utils;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.properties.EncryptableProperties;

import java.io.IOException;
import java.util.Properties;

public class EncryptUtil {

    public static String getEncryptedVaue(String activeProfile, String propertyName) throws IOException {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword("ntVGNDZI7xHKjHHJEqyLGJDRMFmPDjRr");
        Properties props = new EncryptableProperties(encryptor);
        props.load(EncryptUtil.class.getClassLoader().getResourceAsStream("application-" + activeProfile + ".properties"));
        return props.getProperty(propertyName);
    }

}
