package com.compositeapps.gia.utils;

import com.compositeapps.gia.enums.EmailAlias;
import com.compositeapps.gia.models.SalesForce.MailFolder;
import com.compositeapps.gia.models.SalesForce.Message;
import com.compositeapps.gia.models.UserSession;
import com.compositeapps.gia.services.retrofit.OutlookService;
import com.compositeapps.gia.services.retrofit.OutlookServiceBuilder;

public class MessageUtil {

    public static EmailAlias getMessageEmailAlias(Message message) {
        UserSession session = UserSession.getInstance();
        EmailAlias alias = null;
        String folderId  = message.getParentFolderId();

        try {

            OutlookService outlookService = OutlookServiceBuilder.getOutlookService(
                    session.getTokenResponse().getAccessToken(), session.getUserEmail());

            MailFolder folder = outlookService.getMailFolderById(folderId).execute().body();
            if (folder != null) {
                String displayName = folder.getDisplayName();

                if (displayName.equalsIgnoreCase(EmailAlias.ANTWERP.toString())) {
                    alias = EmailAlias.ANTWERP;
                } else if (displayName.equalsIgnoreCase(EmailAlias.BANGKOK.toString())) {
                    alias = EmailAlias.BANGKOK;
                } else if (displayName.equalsIgnoreCase(EmailAlias.BOTSWANA.toString())) {
                    alias = EmailAlias.BOTSWANA;
                } else if (displayName.equalsIgnoreCase(EmailAlias.CARLSBAD.toString())) {
                    alias = EmailAlias.CARLSBAD;
                } else if (displayName.equalsIgnoreCase(EmailAlias.DUBAI.toString())) {
                    alias = EmailAlias.DUBAI;
                } else if (displayName.equalsIgnoreCase(EmailAlias.HONG_KONG.toString())) {
                    alias = EmailAlias.HONG_KONG;
                } else if (displayName.equalsIgnoreCase(EmailAlias.MUMBAI.toString())) {
                    alias = EmailAlias.MUMBAI;
                } else if (displayName.equalsIgnoreCase(EmailAlias.NEW_YORK.toString())) {
                    alias = EmailAlias.NEW_YORK;
                } else if (displayName.equalsIgnoreCase(EmailAlias.RAMAT_GAN.toString())) {
                    alias = EmailAlias.RAMAT_GAN;
                } else if (displayName.equalsIgnoreCase(EmailAlias.SOUTH_AFRICA.toString())) {
                    alias = EmailAlias.SOUTH_AFRICA;
                } else if (displayName.equalsIgnoreCase(EmailAlias.SURAT.toString())) {
                    alias = EmailAlias.SURAT;
                } else if (displayName.equalsIgnoreCase(EmailAlias.TOKYO.toString())) {
                    alias = EmailAlias.TOKYO;
                } else if (displayName.equalsIgnoreCase(EmailAlias.SAM_GIA_TEST.toString())) {
                    alias = EmailAlias.SAM_GIA_TEST;
                } else {
                    alias = EmailAlias.GIA_SALESFORCE;
                }

            }

        } catch (Exception e) {
            e.getMessage();
        }
        return alias;
    }

}
